/**
 * Created by mohana.propa on 11/20/2023.
 */
public class Soccer extends Sports{
    @Override
    String getName(){
        return "Soccer Class";
    }
    @Override
    void getNumberOfTeamMembers(){
        System.out.println( "Each team has "+ 11 +" players in " + getName() );
    }
}
